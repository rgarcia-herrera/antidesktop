all: environment packages

environment:
	ln -s `pwd`/bin/ ~/bin
	ln -s `pwd`/Xdefaults ~/.Xdefaults
	ln -s `pwd`/xinitrc ~/.xinitrc
	ln -s `pwd`/emacs ~/.emacs

	mkdir -p ~/.config/i3
	ln -s `pwd`/i3config ~/.config/i3/config


clean:
	rm ~/bin
	rm ~/.Xdefaults
	rm ~/.xinitrc
	rm ~/.emacs
	rm ~/.config/i3/config


keyboard:
	sudo sed -i 's/XKBLAYOUT="latam"/XKBLAYOUT="us,es"/' /etc/default/keyboard
	sudo sed -i 's/XKBOPTIONS=""/XKBOPTIONS="ctrl:nocaps,grp:shifts_toggle"/' /etc/default/keyboard

grub:
	sudo sed -i 's/#GRUB_TERMINAL=console/GRUB_TERMINAL=console/' /etc/default/grub
	sudo update-grub

dm:
	sudo systemctl disable lightdm
	sudo systemctl set-default multi-user.target

update-apt:
	sudo apt-get update

packages: update-apt
	sudo apt-get install sshfs vlc xterm xtermcontrol i3 xstarfish cplay mpg123 byobu wicd wicd-curses wicd-cli apt-file bash-completion xapm arandr sqlite3 libsqlite3-dev inkscape gimp pandoc chromium-browser kde-spectacle fonts-inconsolata fonts-jura 
	sudo apt-file update 

