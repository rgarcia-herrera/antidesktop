(require 'package)
(add-to-list 'package-archives
	     '("elpy" . "http://jorgenschaefer.github.io/packages/")
             '("melpa" . "https://melpa.org/packages/"))


(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/") t)


(require 'ido)
(ido-mode t)

;;(package-initialize)
;; (elpy-enable)


;; Setup load-path, autoloads and your lisp system
;; Not needed if you install SLIME via MELPA
;; (add-to-list 'load-path "~/slime")
;; (require 'slime-autoloads)
;; (load (expand-file-name "~/quicklisp/slime-helper.el"))

;; (setq inferior-lisp-program "sbcl")
;; (setq slime-contribs '(slime-fancy))


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#FAFAFA" "#FF1744" "#66BB6A" "#F57F17" "#42A5F5" "#7E57C2" "#0097A7" "#546E7A"])
 '(beacon-color "#F8BBD0")
 '(custom-enabled-themes (quote (faff)))
 '(custom-safe-themes
   (quote
    ("70f5a47eb08fe7a4ccb88e2550d377ce085fedce81cf30c56e3077f95a2909f2" "c3e6b52caa77cb09c049d3c973798bc64b5c43cc437d449eacf35b3e776bf85c" "5a0eee1070a4fc64268f008a4c7abfda32d912118e080e18c3c865ef864d1bea" "dd2346baba899fa7eee2bba4936cfcdf30ca55cdc2df0a1a4c9808320c4d4b22" "e4a8ff0d3eaf3076c662ed3cd0a31efac56b93462e1185f04e14e7a0f3abd92b" default)))
 '(evil-emacs-state-cursor (quote ("#D50000" hbar)))
 '(evil-insert-state-cursor (quote ("#D50000" bar)))
 '(evil-normal-state-cursor (quote ("#F57F17" box)))
 '(evil-visual-state-cursor (quote ("#66BB6A" box)))
 '(highlight-indent-guides-auto-enabled nil)
 '(highlight-symbol-colors
   (quote
    ("#F57F17" "#66BB6A" "#0097A7" "#42A5F5" "#7E57C2" "#D84315")))
 '(highlight-symbol-foreground-color "#546E7A")
 '(highlight-tail-colors (quote (("#F8BBD0" . 0) ("#FAFAFA" . 100))))
 '(ido-grid-mode t)
 '(inhibit-startup-screen t)
 '(menu-bar-mode nil)
 '(mouse-yank-at-point t)
 '(package-selected-packages
   (quote
    (abyss-theme apropospriate-theme markdown-mode magit faff-theme elpy)))
 '(pos-tip-background-color "#ffffff")
 '(pos-tip-foreground-color "#78909C")
 '(python-shell-interpreter "ipython")
 '(python-shell-interpreter-args "-i --simple-prompt --pprint")
 '(scroll-bar-mode nil)
 '(tabbar-background-color "#ffffff")
 '(tool-bar-mode nil))
(put 'scroll-left 'disabled nil)


(put 'narrow-to-region 'disabled nil)
(global-set-key [C-tab] 'other-window)

(defun randoname ()
  (interactive)
  (insert (format "%07d" (random 10000000))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 130 :width normal :foundry "GOOG" :family "Inconsolata")))))
